import copy
import re
from io import StringIO

import numpy as np

import pandas as pd

from ..network import Network, Node, Pipe, Pump, Reservoir, Tank, Valve
from ..util import lcmm


class EpanetNetwork(Network):

    @staticmethod
    def __parse_sections(txt, sep):
        # There's generally two types of sections:
        # - Key-value sections
        # - Dataframes
        # Most dataframe sections we can directly parse with Pandas, but some
        # sections are weird and need some preprocessing.
        dataframe_sections = {
            'PIPES': ["id", "node1", "node2", "length", "diameter", "roughness", "minor_loss", "status"],
            'JUNCTIONS': ["id", "elevation", "demand", "pattern_id"],
            'TANKS': ["id", "elevation", "lvl_init", "lvl_min", "lvl_max", "diameter", "vol_min", "vol_curve"],
            'RESERVOIRS': ["id", "head", "pattern"],
            'CURVES': ["id", "x", "y"],
            'DEMANDS': ["node_id", "demand", "pattern_id"],
            'VALVES': ["id", "node1", "node2", "diameter", "type", "setting", "minor_loss_coeff"],
            'PUMPS': ["id", "node1", "node2"]
        }

        # When we have to do a lot of .loc lookups, and we do not need to do
        # any DataFrame like processing, it's much (~100x) faster to just have
        # a dictionary that immediately returns the value we need.
        dict_sections = {
            'COORDINATES': ["nodeid", "xcoord", "ycoord"],
            'VERTICES': ["pipe_id", "xcoord", "ycoord"],
        }

        key_value_sections = ['OPTIONS', 'STATUS']

        # Sections that are not relevant to the skeletonizer, but can be
        # copied as is (should we output to EPANET again).
        metadata_sections = ['ENERGY', 'CONTROLS', 'RULES', 'QUALITY', 'REACTIONS', 'SOURCES',
                             'MIXING', 'TIMES', 'REPORT', 'LABELS', 'BACKDROP', 'TAGS']

        def get_section_text(s):
            """
            Look up ugly EPANET formatted text for a section, and return it in proper CSV-like format.
            Comments and trailing whitespace (per cell) are removed.
            """
            pattern = rf"\[{s}\]\s*(.*?)^\s*\["
            ret = re.search(pattern, txt, re.DOTALL | re.MULTILINE).group(1)

            lines = []
            for line in ret.split('\n'):
                try:
                    line = line[:line.index(';')]  # Strip comment
                except ValueError:
                    # No comment in line
                    pass

                line = line.strip()
                if not line:
                    continue

                line = "\t".join([x.strip() for x in line.split(sep)])
                lines.append(line)

            return "\n".join(lines)

        sections = {}

        for s, colnames in dataframe_sections.items():
            section_txt = get_section_text(s)

            # Pumps has a special last column, which consists of multiple values separated by the same separator.
            # We strip that field for now, as we do not need it.
            if s == "PUMPS":
                lines = [re.split(sep, l, maxsplit=3)[:3] for l in get_section_text(s).split('\n')]
                section_txt = "\n".join((sep.join(x) for x in lines))

            df = pd.read_csv(StringIO(section_txt), names=colnames, sep=sep, comment=';')
            sections[s] = df.set_index(colnames[0])

        for s in key_value_sections:
            section_txt = get_section_text(s)

            if not section_txt.strip():
                sections[s] = {}
                continue
            else:
                sections[s] = {a.strip().lower(): b.strip() for a, b in
                               (x.strip().split(sep) for x in section_txt.split('\n'))}

        for s in dict_sections:
            section_txt = get_section_text(s)

            if not section_txt.strip():
                sections[s] = {}
                continue
            else:
                if s == "COORDINATES":
                    # We only have one entry per key
                    sections[s] = {a.strip(): (float(b), float(c)) for a, b, c in
                                   (x.strip().split(sep) for x in section_txt.split('\n'))}

                elif s == "VERTICES":
                    sections[s] = {}
                    # Multiple entries per key, which we should append in a list
                    for x in section_txt.split('\n'):
                        a, b, c = x.strip().split(sep)
                        a, b, c = a.strip(), float(b), float(c)

                        sections[s].setdefault(a, []).append((b, c))
                else:
                    raise Exception(f"Unknown dictionary section '{s}'")

        # Patterns section
        s = "PATTERNS"
        section_txt = get_section_text(s)
        patterns = {}
        for l in section_txt.split("\n"):
            p, *v = l.split(sep)
            p = p.strip()
            v = [float(x.strip()) for x in v]
            patterns.setdefault(p, []).extend(v)
        sections[s] = patterns

        ignored_sections = {}
        for s in metadata_sections:
            ignored_sections[s] = get_section_text(s)

        return sections, ignored_sections

    @classmethod
    def read(cls, fname, sep='\t', squash="maxsum"):
        with open(fname) as f:
            txt = f.read()

        sections, ignored_sections = cls.__parse_sections(txt, sep)

        if float(sections["OPTIONS"].get('demand multiplier', 1.0)) != 1.0:
            raise NotImplementedError("Demand multipliers other than 1.0 are not supported")

        # First we have to squash demands, as patterns are not supported
        junctions, patterns, demands = cls.__squash_demands(
            sections["JUNCTIONS"], sections["DEMANDS"], sections["PATTERNS"],
            default_pattern=sections["OPTIONS"].get('pattern', None), flow=squash)

        # Overwrite original values
        sections["JUNCTIONS"] = junctions
        sections["PATTERNS"] = patterns
        sections["DEMANDS"] = demands

        # Make nodes based on squashed data
        nodes = {}
        objects = {}

        for name, elevation, demand, _ in sections["JUNCTIONS"].itertuples():
            coordinates = sections["COORDINATES"][name]
            nodes[name] = Node(name=name, demand=demand, elevation=elevation, coordinates=coordinates)

        # Tanks are a node and element in one
        for name, elevation, lvl_init, lvl_min, lvl_max, diameter, vol_min, _ in sections["TANKS"].itertuples():
            coordinates = sections["COORDINATES"][name]
            nodes[name] = Node(name=name, elevation=elevation, coordinates=coordinates)
            objects[name] = Tank(name=name, node=nodes[name], elevation=elevation, initial_level=lvl_init,
                                 diameter=diameter/1000.0, minimum_level=lvl_min, maximum_level=lvl_max,
                                 minimum_volume=vol_min)

        # Reservoirs are a node and element in one
        for name, head, _ in sections["RESERVOIRS"].itertuples():
            coordinates = sections["COORDINATES"][name]
            nodes[name] = Node(name=name, elevation=head, coordinates=coordinates)
            objects[name] = Reservoir(name=name, node=nodes[name], head=head)

        for name, node1, node2 in sections["PUMPS"].itertuples():
            objects[name] = Pump(name=name, start_node=nodes[node1], end_node=nodes[node2])

        # TODO: Shut-off valves and check valves are part of a pipe. This
        # section just  contains control valves of some sort. We currently do
        # not distinguish between open and active control valves.
        for name, node1, node2, diameter, type_, _, _ in sections["VALVES"].itertuples():
            opened = True
            if name in sections["STATUS"]:
                status = sections["STATUS"][name].lower().strip()
                if status == "closed":
                    opened = False
                elif status in ("open", "active"):
                    pass
                else:
                    raise ValueError(f"Valve '{name}' has unknown valve status '{status}'")
            objects[name] = Valve(name=name, start_node=nodes[node1], end_node=nodes[node2],
                                  diameter=diameter/1000.0, valve_type=type_, opened=opened)

        pipes = {}

        for name, node1, node2, length, diameter, roughness, _, _ in sections["PIPES"].itertuples():
            vertices = sections["VERTICES"].get(name, [])

            if name in sections["STATUS"]:
                raise NotImplementedError("Setting a status on a pipe is not yet supported.")

            pipes[name] = Pipe(name=name,
                               start_node=nodes[node1],
                               end_node=nodes[node2],
                               length=length,
                               diameter=diameter/1000.0,
                               roughness=roughness/1000.0,
                               vertices=vertices)

        # Demands to SI units (m3/s)
        factor_to_si = {
            "CFS": 0.0283168,
            "GPM": 6.30902e-5,
            "MGD": 0.0438126364,
            "IMGD": 0.0526167824,
            "AFD": 0.0142764,
            "LPS": 1E-3,
            "LPM": 1.66667e-5,
            "MLD": 0.0115740741,
            "CMH": 1/3600,
            "CMD": 1/86400
        }
        factor = factor_to_si[sections["OPTIONS"].get("units", "GPM")]

        for n in nodes.values():
            n.demand *= factor

        # TODO: Headloss/friction coefficient unit conversion

        network = cls()
        network.add_nodes(*nodes.values())
        network.add_pipes(*pipes.values())
        network.add_objects(*objects.values())

        network.__options = sections["OPTIONS"]
        network.__metadata_sections = ignored_sections

        return network

    @staticmethod
    def write(network, fname):
        """
        NOTE: A maximum of 255 characters can appear on a line. The ID labels used
        to identify nodes, links, curves and patterns can be any combination
        of up to 15 characters and numbers.
        """

        if not fname.endswith('.inp'):
            fname += ".inp"

        # TODO: Simplify and DRY with information already specified in read()
        # method, e.g. the "dataframe" section.
        with open(fname, 'w') as o:
            # Group network objects type
            objects = {}
            for obj in network.objects:
                k = type(obj)
                objects.setdefault(k, []).append(obj)

            # Note that Tanks and Reservoirs are like Junctions in EPANET,
            # whereas we stored them as a separate object and node. We
            # therefore write them out with the node's name, such that
            # references by other compenents remain valid.
            skip_nodes = []

            o.write("\n[TANKS]\n")
            for t in objects.get(Tank, []):
                o.write(f"{t.node.name}\t{t.elevation}\t{t.initial_level}\t{t.minimum_level}\t"
                        f"{t.maximum_level}\t{t.diameter*1000}\t{t.minimum_volume}\t\t\n")
                skip_nodes.append(t.node)

            o.write("\n[RESERVOIRS]\n")
            for t in objects.get(Reservoir, []):
                o.write(f"{t.node.name}\t{t.head}\t\n")
                skip_nodes.append(t.node)

            # Node data
            o.write("\n[JUNCTIONS]\n")
            for n in network.nodes:
                if n in skip_nodes:
                    skip_nodes.remove(n)
                    continue
                o.write(f"{n.name}\t{n.elevation}\t{n.demand}\t\n")

            assert len(skip_nodes) == 0, "Not all tanks/reservoirs were connected to equally named nodes"

            o.write("\n[COORDINATES]\n")
            for n in network.nodes:
                o.write(f"{n.name}\t{n.coordinates[0]}\t{n.coordinates[1]}\n")

            # Demands/patterns are not supported

            # Pipe data
            o.write("\n[PIPES]\n")
            for p in network.pipes:
                o.write(f"{p.name}\t{p.start_node.name}\t{p.end_node.name}\t{p.length}\t"
                        f"{p.diameter*1000}\t{p.roughness*1000}\t\t\n")

            o.write("\n[VERTICES]\n")
            for p in network.pipes:
                for vx, vy in p.vertices:
                    o.write(f"{p.name}\t{vx}\t{vy}\n")

            # Data of other network objects
            # Patterns is skipped, because demand is constant.
            o.write("\n[PUMPS]\n")
            for p in objects[Pump]:
                o.write(f"{p.name}\t{p.start_node}\t{p.end_node}\n")

            o.write("\n[VALVES]\n")
            for v in objects[Valve]:
                o.write(f"{v.name}\t{v.start_node}\t{v.end_node}\t{v.diameter*1000}\t{v.valve_type}\n")

            # Status of pipes, pumps and valves (i.e. open/active or closed)
            o.write("\n[STATUS]\n")
            for v in objects[Valve]:
                if not v.opened:
                    o.write(f"{v.name}\tClosed\n")

            # Base pattern name might have changed, so we cannot blindly copy over
            o.write("\n[OPTIONS]\n")
            for k, v in network.__options.items():
                o.write(f"{k}\t{v}\n")

            # Copy over sections if the input also happened to be epanet.
            for k, v in network.__metadata_sections.items():
                o.write(f"\n[{k}]\n{v}\n")

    @staticmethod
    def __squash_demands(junctions, demands, patterns, default_pattern=None, flow="maxsum"):
        """
        Squash the base demand (with pattern) and/or any further demands (with
        pattern) into a single constant demand.
        """

        if np.all(pd.isnull(junctions.pattern_id)) and demands.empty:
            # Nothing to squash
            return junctions, {}, demands

        # Make copy of junction, to avoid modifying input argument
        junctions = junctions.copy()

        # The demands section overrules the demand of a junction, if there is
        # an entry for said junction in the demands section.
        demand_junctions = set(demands.index)
        junctions.loc[demand_junctions, 'demand'] = 0.0

        # To make it easy for ourselves, we move any non-zero demands of junctions to the demands section
        non_zero_junc = junctions[junctions.demand > 0.0]
        demands = pd.concat([demands, non_zero_junc[['demand', 'pattern_id']]])
        junctions.loc[junctions.demand > 0.0, 'demand'] = 0.0

        if default_pattern is not None:
            junctions.fillna({'pattern_id': default_pattern}, inplace=True)
            demands.fillna({'pattern_id': default_pattern}, inplace=True)

        # Squash the pattern-based demands to a single demand
        squash_factors = {}

        if flow in ["maxmax", "avgavg", "minmin"]:
            # Easy to determine, as the only dependency is the pattern itself.
            for p in patterns:
                if flow == "maxmax":
                    squash_factors[p] = max(patterns[p])
                elif flow == "minmin":
                    squash_factors[p] = min(patterns[p])
                elif flow == "avgavg":
                    squash_factors[p] = sum(patterns[p])/len(patterns[p])
        else:
            # We also need to sum up all the demands, to see what at what
            # moment in time the flow is highest/lowest.
            sum_patterns = demands.groupby('pattern_id').sum().to_dict()['demand']

            # Equal length patterns
            eq_patterns = copy.deepcopy(patterns)
            ptn_lengths = [len(eq_patterns[x]) for x in eq_patterns]
            new_length = lcmm(*ptn_lengths)

            for p in eq_patterns:
                eq_patterns[p] = np.array(
                    [x for x in eq_patterns[p] for i in range(int(new_length/len(eq_patterns[p])))])

            # If patterns are present in the junctions/demand, but were not
            # specified in the [PATTERNS] sections, set value to flat (i.e. [1.0]*N)
            for ptn in sum_patterns:
                if ptn not in eq_patterns:
                    eq_patterns[ptn] = np.ones(new_length)

            # Find the time of day where min/avg/max flow occurs
            values = np.zeros(new_length)

            for ptn in sum_patterns:
                ptn_mult = sum_patterns[ptn] * eq_patterns[ptn]
                values += ptn_mult

            if flow == "maxsum":
                i = np.argmax(values)
            elif flow == "minsum":
                i = np.argmin(values)
            elif flow == "avgsum":
                # Find time of day where value is closest to average
                avg = np.mean(values)
                values = np.abs(values - avg)
                i = np.argmin(values)

            for p in eq_patterns:
                squash_factors[p] = eq_patterns[p][i]

        demands['sq_demand'] = np.nan
        for p in eq_patterns:
            ind = (demands.pattern_id == p)
            demands.loc[ind, 'sq_demand'] = demands.loc[ind, 'demand'] * squash_factors[p]

        new_demands = demands['sq_demand'].reset_index().groupby('index').sum()['sq_demand']
        junctions.loc[new_demands.index, 'demand'] = new_demands.values
        junctions['pattern_id'] = np.nan

        # Demands and patterns are gone
        patterns = {}
        demands = demands.drop('sq_demand', axis='columns').iloc[0:0]

        return junctions, patterns, demands

    def can_remove_node(self, n):
        # With the way Epanet models are read in and exported, we should not
        # allow nodes connected to Tanks and Reservoirs to be removed.
        can_remove = super().can_remove_node(n)

        if not hasattr(self, '_tank_reservoir_nodes'):
            self._tank_reservoir_nodes = {x.name for x in self.objects
                                          if isinstance(x, Tank) or isinstance(x, Reservoir)}

        return can_remove and n.name not in self._tank_reservoir_nodes
