from math import inf

import numpy as np

import pywanda as wd

from ..network import Network


class WandaNetwork(Network):

    @classmethod
    def read(cls, fname):
        raise NotImplementedError

    @staticmethod
    def write(network, fname, wanda_bin, dp_reopen=1E4, pressure_downstream=3E4, scale_factor=5000):
        # Determining the size of the model and required scaling
        min_x, min_y = inf, inf
        max_x, max_y = -inf, -inf

        for x, y in (n.coordinates for n in network.nodes):
            min_x, min_y = min(min_x, x), min(min_y, y)
            max_x, max_y = max(max_x, x), max(max_y, y)

        scaling = max(scale_factor / (max_x - min_x), scale_factor / (max_y - min_y))
        offset = np.array([min_x, max_y]) * scaling

        if not wanda_bin.endswith("\\"):
            wanda_bin += "\\"
        model = wd.WandaModel(fname, wanda_bin)

        wd_nodes = {}
        wd_taps = {}
        wd_pipes = {}

        for n in network.nodes:
            pos = tuple(np.array(n.coordinates) * scaling - offset)
            wn = wd_nodes[n] = model.add_node("Hydraulic node", pos)
            wt = wd_taps[n] = model.add_component("Tap non-return flow DP reopen", pos)

            model.connect(wt, 1, wn)

            wn.get_property("Elevation").set_scalar(n.elevation)
            wn.set_name(n.name[:16])

            wt.get_property("Downstream pressure").set_scalar(pressure_downstream)
            wt.get_property("Initial delivery rate").set_scalar(n.demand)
            wt.get_property("Delta P for reopen").set_scalar(dp_reopen)
            wt.set_name(n.name[:16])

        for p in network.pipes:
            pos = (np.array(p.start_node.coordinates) + np.array(p.start_node.coordinates)) / 2.0
            pos = tuple(pos * scaling - offset)
            wp = wd_pipes[p] = model.add_component("Pipe (Liquid)", pos)

            wp.set_name(p.name[:16])
            wp.get_property("Length").set_scalar(p.length)
            wp.get_property("Inner diameter").set_scalar(p.diameter)
            wp.get_property("Wall roughness").set_scalar(p.roughness)
            model.connect(wp, 1, wd_nodes[p.start_node])
            model.connect(wp, 2, wd_nodes[p.end_node])

        model.save_model_input()
        model.close()
