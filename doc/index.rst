.. Skeletonizer documentation master file, created by
   sphinx-quickstart on Sun Feb 25 23:22:08 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Hydraulic Network Skeletonizer's documentation!
==============================================================

Contents:
=========

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   getting-started
   support

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   python-api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
